# -*- Mode: makefile -*-

include ../config-host.mak
include config-target.mak
include config-devices.mak
include $(SRC_PATH)/rules.mak

$(call set-vpath, $(SRC_PATH))
cflags-$(CONFIG_LINUX) += -I../linux-headers
cflags-y += -I.. -I$(SRC_PATH)/target-$(TARGET_BASE_ARCH) -DNEED_CPU_H

cflags-y+=-I$(SRC_PATH)/include

ifdef CONFIG_USER_ONLY
# user emulator name
QEMU_PROG=qemu-$(TARGET_ARCH2)
else
# system emulator name
ifneq (,$(findstring -mwindows,$(libs_softmmu)))
# Terminate program name with a 'w' because the linker builds a windows executable.
QEMU_PROGW=qemu-system-$(TARGET_ARCH2)w$(EXESUF)
endif # windows executable
QEMU_PROG=qemu-system-$(TARGET_ARCH2)$(EXESUF)
endif

PROGS=$(QEMU_PROG)
ifdef QEMU_PROGW
PROGS+=$(QEMU_PROGW)
endif
STPFILES=

config-target.h: config-target.h-timestamp
config-target.h-timestamp: config-target.mak

ifdef CONFIG_TRACE_SYSTEMTAP
stap: $(QEMU_PROG).stp

ifdef CONFIG_USER_ONLY
TARGET_TYPE=user
else
TARGET_TYPE=system
endif

$(QEMU_PROG).stp: $(SRC_PATH)/trace-events
	$(call quiet-command,$(TRACETOOL) \
		--format=stap \
		--backend=$(TRACE_BACKEND) \
		--binary=$(bindir)/$(QEMU_PROG) \
		--target-arch=$(TARGET_ARCH) \
		--target-type=$(TARGET_TYPE) \
		< $< > $@,"  GEN   $(TARGET_DIR)$(QEMU_PROG).stp")
else
stap:
endif

all: $(PROGS) stap

# Dummy command so that make thinks it has done something
	@true

CONFIG_NO_PCI = $(if $(subst n,,$(CONFIG_PCI)),n,y)
CONFIG_NO_KVM = $(if $(subst n,,$(CONFIG_KVM)),n,y)
CONFIG_NO_XEN = $(if $(subst n,,$(CONFIG_XEN)),n,y)
CONFIG_NO_GET_MEMORY_MAPPING = $(if $(subst n,,$(CONFIG_HAVE_GET_MEMORY_MAPPING)),n,y)
CONFIG_NO_CORE_DUMP = $(if $(subst n,,$(CONFIG_HAVE_CORE_DUMP)),n,y)

#########################################################
# cpu emulator library
obj-y = exec.o translate-all.o cpu-exec.o
obj-y += tcg/tcg.o tcg/optimize.o
obj-$(CONFIG_TCG_INTERPRETER) += tci.o
obj-$(CONFIG_TCG_INTERPRETER) += disas/tci.o
obj-y += fpu/softfloat.o
obj-y += target-$(TARGET_BASE_ARCH)/
obj-y += disas.o
obj-$(CONFIG_GDBSTUB_XML) += gdbstub-xml.o
obj-$(CONFIG_NO_KVM) += kvm-stub.o

#########################################################
# Linux user emulator target

cflags-$(CONFIG_LINUX_USER) += -I$(SRC_PATH)/linux-user/$(TARGET_ABI_DIR) -I$(SRC_PATH)/linux-user
obj-$(CONFIG_LINUX_USER) += linux-user/
obj-$(CONFIG_LINUX_USER) += gdbstub.o thunk.o user-exec.o

#########################################################
# BSD user emulator target

cflags-$(CONFIG_BSD_USER) += -I$(SRC_PATH)/bsd-user -I$(SRC_PATH)/bsd-user/$(TARGET_ARCH)
obj-$(CONFIG_BSD_USER) += bsd-user/
obj-$(CONFIG_BSD_USER) += gdbstub.o user-exec.o

#########################################################
# System emulator target
obj-$(CONFIG_SOFTMMU) += arch_init.o cpus.o monitor.o gdbstub.o balloon.o ioport.o
obj-$(CONFIG_SOFTMMU) += qtest.o
obj-$(CONFIG_SOFTMMU) += hw/
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_FDT)) += device_tree.o
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_KVM)) += kvm-all.o
obj-$(CONFIG_SOFTMMU) += memory.o savevm.o cputlb.o
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_HAVE_GET_MEMORY_MAPPING)) += memory_mapping.o
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_HAVE_CORE_DUMP)) += dump.o
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_NO_GET_MEMORY_MAPPING)) += memory_mapping-stub.o
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_NO_CORE_DUMP)) += dump-stub.o

# xen support
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_XEN)) += xen-all.o xen-mapcache.o
obj-$(call land,$(CONFIG_SOFTMMU),$(CONFIG_NO_XEN)) += xen-stub.o

# Hardware support
obj-$(call land,$(CONFIG_SOFTMMU),$(call eq,$(TARGET_ARCH),sparc64)) += hw/sparc64/
obj-$(call land,$(CONFIG_SOFTMMU),$(call ne,$(TARGET_ARCH),sparc64)) += hw/$(TARGET_BASE_ARCH)/

main.o: cflags-$(CONFIG_SOFTMMU)+=$(GPROF_CFLAGS)

libs-y+=$(libs_softmmu)

GENERATED_HEADERS += $(if CONFIG_SOFTMMU,hmp-commands.h qmp-commands-old.h)

# Workaround for http://gcc.gnu.org/PR55489, see configure.
%/translate.o: cflags-y += $(TRANSLATE_OPT_CFLAGS)

nested-vars += obj-y

# This resolves all nested paths, so it must come last
include $(SRC_PATH)/Makefile.objs

all-obj-y = $(obj-y)
all-obj-y += $(addprefix ../, $(common-obj-y))

ifndef CONFIG_HAIKU
libs-y+=-lm
endif

ifdef QEMU_PROGW
# The linker builds a windows executable. Make also a console executable.
$(QEMU_PROGW): $(all-obj-y) ../libqemuutil.a ../libqemustub.a
	$(call LINK,$^)
$(QEMU_PROG): $(QEMU_PROGW)
	$(call quiet-command,$(OBJCOPY) --subsystem console $(QEMU_PROGW) $(QEMU_PROG),"  GEN   $(TARGET_DIR)$(QEMU_PROG)")
else
$(QEMU_PROG): $(all-obj-y) ../libqemuutil.a ../libqemustub.a
	$(call LINK,$^)
endif

gdbstub-xml.c: $(TARGET_XML_FILES) $(SRC_PATH)/scripts/feature_to_c.sh
	$(call quiet-command,rm -f $@ && $(SHELL) $(SRC_PATH)/scripts/feature_to_c.sh $@ $(TARGET_XML_FILES),"  GEN   $(TARGET_DIR)$@")

hmp-commands.h: $(SRC_PATH)/hmp-commands.hx
	$(call quiet-command,sh $(SRC_PATH)/scripts/hxtool -h < $< > $@,"  GEN   $(TARGET_DIR)$@")

qmp-commands-old.h: $(SRC_PATH)/qmp-commands.hx
	$(call quiet-command,sh $(SRC_PATH)/scripts/hxtool -h < $< > $@,"  GEN   $(TARGET_DIR)$@")

clean:
	rm -f *.a *~ $(PROGS)
	rm -f $(shell find . -name '*.[od]')
	rm -f hmp-commands.h qmp-commands-old.h gdbstub-xml.c
ifdef CONFIG_TRACE_SYSTEMTAP
	rm -f *.stp
endif

install: all
ifneq ($(PROGS),)
	$(INSTALL) -m 755 $(PROGS) "$(DESTDIR)$(bindir)"
ifneq ($(STRIP),)
	$(STRIP) $(patsubst %,"$(DESTDIR)$(bindir)/%",$(PROGS))
endif
endif
ifdef CONFIG_TRACE_SYSTEMTAP
	$(INSTALL_DIR) "$(DESTDIR)$(qemu_datadir)/../systemtap/tapset"
	$(INSTALL_DATA) $(QEMU_PROG).stp "$(DESTDIR)$(qemu_datadir)/../systemtap/tapset"
endif

GENERATED_HEADERS += config-target.h
Makefile: $(GENERATED_HEADERS)
